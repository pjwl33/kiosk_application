module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    nodewebkit: {
      options: {
        version: '0.8.6',
        platforms: ['win32','linux','osx32'],
        buildDir: './build',
        winIco: 'public/icons/icon.ico',
      },
      src: ['./public/**/*']
    },
    shell: {
      fixnw: {
        command: "sed -i 's/\x75\x64\x65\x76\x2E\x73\x6F\x2E\x30/\x75\x64\x65\x76\x2E\x73\x6F\x2E\x31/g' build/kiosk/linux64/kiosk"
      }
    }
  });

  grunt.loadNpmTasks('grunt-node-webkit-builder');
  grunt.registerTask('default', ['nodewebkit', 'shell:fixnw']);

};