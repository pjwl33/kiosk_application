kiosk.controller('CheckInCtrl', function($scope, $timeout, dymoPrinter) {
  $scope.goBack = function() {
    window.history.back();
  };

  // if coming from Tenant Directory page
  $scope.tenant = app.getStore('tenant_call');
  $scope.visitorName = app.getStore('visitor_name');
  if ($scope.tenant) {
    createNewVisit();
  } else {
    $scope.tenant = {
      id: null,
      name: "HOST TENANT",
      suite: "Booth 6039 ISC West"
    };
  }

  $scope.visit = app.getStore('selected_schedule') ? app.getStore('selected_schedule') : $scope.newVisit;

  $scope.ndaText = NDA_TEXT({
    today_date: moment().format('L'),
    company: $scope.visit.tenant.name,
    visitor_name: $scope.visit.visitor.name
  });

  $scope.checkInText = "Please read and sign the non-disclosure agreement below.";

  Webcam(function(data) {
    var info = $scope.visit;
    if (info) info.image = data.image.src;

    $scope.$apply(function() {
      $scope.badgeInfo = info;
      $scope.showBackBtn = true;
    });

    $scope.$on('$destroy', function() {
      data.shutDown();
      $('body').children().unbind();
    });
  });

  $timeout(function() {
    Animations.singleOverlayInit({
      overlay_id: 'nda-overlay'
    }, function(data) {
      $scope.overlayToggle = function() {
        data.toggleOverlay(data.overlay);
      };
    });
  });

  function createNewVisit() {
    $scope.newVisit = {
      barcode: "Hello Cathy",
      visit_start_time: moment().unix(),
      visit_end_time: moment().add(1, 'hours').unix(),
      visitor: {
        first_name: "",
        last_name: "",
        name: $scope.visitorName
      },
      tenant: {
        id: $scope.tenant.company_id,
        name: $scope.tenant.company_name,
        suite: "Booth 6039 ISC West",
      },
      tenant_contact: {
        id: $scope.tenant.id,
        first_name: $scope.tenant.first_name,
        last_name: $scope.tenant.last_name,
        name: $scope.tenant.name
      }
    };
  }

  $scope.showNDA = function() {
    $scope.overlayToggle();

    var height = $(document).height();
    $('.nda-box').height(height * 0.33);

    var canvas = document.querySelector('#signature');
    $scope.signaturePad = new SignaturePad(canvas, {
      penColor: 'white',
    });
  };

  $scope.agreeNDA = function() {
    if ($scope.visit.id) {
      AppService.checkInVisit({
        visit_id: $scope.visit.id
      }, function(data) {
        console.log(data);
        $scope.printBadge($scope.badgeInfo);
      }, function(data, errorText) {
        // errorHandler();
        $scope.printBadge($scope.badgeInfo);
      });
    } else {
      $scope.printBadge($scope.badgeInfo);
    }

    // function errorHandler() {
    //   $scope.checkInText = "Unable to check in at this time. Please visit a guard for assistance.";
    //   return;
    // }
  };

  $scope.declineNDA = function() {
    $scope.overlayToggle();
    location.href = "#/";
  };

  $scope.printBadge = function(info) {
    dymoPrinter.print($scope.badgeInfo);
    location.href = "#/";
  };

});