kiosk.controller('TenantCallCtrl', function($scope, $timeout, $interval, autoDial) {
  $scope.goBack = function() {
    location.href = '#/';
  };
  var callTimeOn;
  $scope.letters = ALPHA_NUMERICAL;
  $scope.numSymbols = NUM_SYMBOLS;
  $scope.optSymbols = OPT_SYMBOLS;
  $scope.queryString = '';
  $scope.visitor = app.getStore('selected_visitor');
  if (app.getStore('selected_schedule')) app.removeStore('selected_schedule');

  $timeout(function() {
    Animations.singleOverlayInit({
      overlay_id: 'phone-overlay'
    }, function(data) {
      $scope.overlayToggle = function() {
        data.toggleOverlay(data.overlay);
      };
    });
  });

  $scope.updateQuery = function(val) {
    if (val == 'SPACE') {
      $scope.queryString += ' ';
    } else if (val == 'DELETE') {
      $scope.queryString = $scope.queryString.slice(0, $scope.queryString.length - 1);
    } else {
      $scope.queryString += val;
    }
  };

  $scope.$watch('queryString', function(val) {
    if (val.length > 2) {
      if ($scope.filterTextTimeout) $timeout.cancel($scope.filterTextTimeout);
      $scope.tempFilterText = val;
      $scope.filterTextTimeout = $timeout(function() {
        $scope.filterQuery = $scope.tempFilterText;
        $scope.searchTenants($scope.filterQuery);
      }, 250);
    }
  });

  $scope.searchTenants = function(query) {
    AppService.fetchTenants({
      query: query
    }, function(data) {
      $scope.$apply(function() {
        $scope.results = data;
      });
      $scope.noResults = $scope.results.length <= 0 ? true : false;
    }, function(data, errorText) {
      console.log(data);
      console.log(errorText);
    });
  };

  $scope.callTenant = function(tenant) {
    $scope.overlayToggle();
    $scope.calling = tenant;
    $scope.callText = 'Calling ' + tenant.company_name + '...';
    $scope.callLength = "00:00:00";
    $scope.timer = 0;
    callTimeOn = $interval(function() { $scope.startCallTimer(); }, 1100);
    if (autoDial.connect()) {
      autoDial.emit('call:dial', {
        from: {
          name: $scope.visitor.name,
          number: ''
        },
        to: {
          name: tenant.name,
          number: tenant.phone_number
        }
      });
      autoDial.on('call:pickup', function(data) {
        $scope.callText = 'Connected to ' + tenant.name;
      });
      autoDial.on('call:noResponse', function(data) {
        $scope.callText = "Coud not connect to " + tenant.name + ". Please try again or see a guard for assistant.";
        delayedHangup();
      });
      autoDial.on('call:hangup', function(data) {
        $scope.callText = tenant.name + " has dropped the call.";
        delayedHangup();
      });
      autoDial.on('call:grant', function(data) {
        $scope.grantAccess();
      });
      autoDial.on('call:deny', function(data) {
        $scope.denyAccess();
      });
    } else {
      $scope.callText = "Could not make an out going call at this time";
      delayedHangup();
    }
  };

  $scope.startCallTimer = function() {
    var seconds = $scope.timer;
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    seconds = seconds >= 60 ? seconds % 60 : seconds;
    minutes = minutes >= 60 ? minutes % 60 : minutes;
    secondsText = seconds >= 10 ? seconds : ("0" + seconds);
    minutesText = minutes >= 10 ? minutes : ("0" + minutes);
    hoursText = hours >= 10 ? hours : ("0" + hours);
    $scope.callLength = hoursText + ":" + minutesText + ":" + secondsText;
    $scope.timer += 1;
  };

  $scope.grantAccess = function() {
    $scope.callText = "Permission granted. Get ready to take your photo.";
    app.setStore('tenant_call', $scope.calling);
    delayedHangup();
    location.href = "#/check_in";
  };

  $scope.denyAccess = function() {
    $scope.callText = "Permission Denied. Please see a security guard for assistance.";
    delayedHangup();
  };

  $scope.hangup = function() {
    $scope.callText = 'Hanging up...';
    $interval.cancel(callTimeOn);
    autoDial.removeAllListeners();
    $timeout(function() {
      $scope.overlayToggle();
    }, 2200);
  };

  function delayedHangup() {
    $timeout(function() { $scope.hangup(); }, 4200);
  }

  $scope.$on('$destroy', function() {
    $('body').children().unbind();
    autoDial.removeAllListeners();
  });
});