kiosk.controller('HomeCtrl', function($scope, $routeParams, $timeout, $interval, dlScanner, motoScanner, pivScanner) {

  $scope.visitor = {};
  $scope.visitorExtraInfo = {}; //i.e. photos, expirations dates, etc.

  $timeout(function() {
    Animations.overlayInit(function(data) {
      $scope.PivCardOverlay = data.overlay;
      $scope.DriverLicenseOverlay = data.overlayTwo;
      $scope.BarcodeOverlay = data.overlayThree;
      initiateConfirmButton();
    });
  }, 3000);

  // DRIVER LICENSE SCANNER
  if (dlScanner.connect()) {
    dlScanner.start();
    dlScanner.removeAllListeners();
    dlScanner.on('scan', function(data) {
      if (data) {
        if (data.doc && data.doc.id) {
          checkLoading('DriverLicense');
          if ($scope.validateBC) validateVisitor(data, 'DriverLicense');
          if (!$scope.visitor.docId) parseVisitor(data, 'DriverLicense');
        }
      }
    });
  }

  // PIV SCANNER
  if (pivScanner.connect()) {
    pivScanner.removeAllListeners();
    pivScanner.on('card:inserted', function(data) {
      if (data) {
        console.log('card inserted');
        checkLoading('PivCard');
      }
    });
    pivScanner.on('card:read', function(data) {
      if (data) {
        checkLoading('PivCard');
        if ($scope.validateBC) validateVisitor(data, 'PivCard');
        if (!$scope.visitor.docId) parseVisitor(data, 'PivCard');
      }
    });
    pivScanner.on('card:removed', function(data) {
      if (data) {
        $scope.noLoading();
        console.log('card removed');
      }
    });
  }

  // MOTO SCANNER
  if (motoScanner.connect()) {
    motoScanner.removeAllListeners();
    motoScanner.on('data', function(data) {
      if (data) {
        checkLoading('Barcode');
        if (!$scope.visitor.docId) parseVisitor(data, 'Barcode');
      }
    });
  }

  function parseVisitor(data, scanType) {
    console.log(data);
    $scope.scanType = scanType;

    switch (scanType) {
      case 'DriverLicense':
        if (data.images) setImages(data.images);
        $scope.visitor.dob = data.doc.dob;
        $scope.visitor.dln = data.doc.id;
        $scope.visitor.docId = data.doc.id;
        $scope.visitor.first_name = data.doc.first_name;
        $scope.visitor.last_name = data.doc.last_name;
        $scope.visitor.name = data.doc.first_name + " " + data.doc.last_name;
        break;
      case 'Barcode':
        $scope.validateBC = true;
        $scope.barcodeValidText = "Please scan your driver's license, passport, or PIV card to validate your ID for this visit.";
        $scope.visitor.barcode = JSON.parse(data).raw;
        $scope.visitor.docId = JSON.parse(data).raw;
        break;
      case 'PivCard':
        $scope.visitor.piv_id = data;
        $scope.visitor.docId = data;
        checkPivCardValidity();
        break;
      default:
        return;
    }
    checkSearchValidity();
  }

  function validateVisitor(data, scanType) {
    switch (scanType) {
      case 'DriverLicense':
        if ($scope.visitor.last_name.toLowerCase() == data.doc.last_name.toLowerCase()) {
          if (data.images) setImages(data.images);
          visitorValid();
        } else {
          $scope.barcodeValidText = 'Id does not match visitor profile. Please try again.';
        }
        break;
      case 'PivCard':
        if (data) visitorValid();
        break;
      default:
        $scope.barcodeValidText = 'Could not detect a valid ID. Please see a guard for assistance.';
    }
    $scope.noLoading();
  }

  function setImages(images) {
    $scope.visitorExtraInfo.front_img_b64 = images.license;
    $scope.visitorExtraInfo.face_img_b64 = images.face;
  }

  function checkPivCardValidity() {
    AppService.validatePivCard({
      data: {
        document_id: $scope.visitor.docId
      }
    }, function(data) {
      $scope.$apply(function() {
        $scope.visitor.name = data.name;
      });
    }, function(data, errorText) {
      console.log(data.status + ": " + errorText);
    });
  }

  function checkSearchValidity() {
    if ($scope.visitor.dln || $scope.visitor.passport_id || $scope.visitor.piv_id || $scope.visitor.barcode) {
      $scope.searchSchedules();
    } else {
      $scope.visitor = {};
    }
  }

  function visitorValid() {
    $scope.validateBC = false;
    $scope.barcodeValidText = "ID Validated. Please confirm your visit to take a photo for your badge.";
  }

  $scope.searchSchedules = function() {
    AppService.fetchSchedules({
      data: {
        document_id: $scope.visitor.docId,
        document_type: $scope.scanType
      }
    }, function(data) {
      console.log(data);
      setSchedule(data);
      if ($scope.scanType !== 'Barcode' || !$scope.noResults) {
        showOverlay();
      } else if ($scope.scanType == 'Barcode' && $scope.noResults) {
        location.reload();
      }
    }, function(data, errorText) {
      location.reload();
    });
  };

  function setSchedule(data) {
    $scope.$apply(function() {
      $scope.allSchedules = data;
      $scope.schedule = _.first($scope.allSchedules);

      console.log($scope.schedule);
      $scope.multipleVisits = data.length > 1 ? true : false;
      $scope.noResults = data.length <= 0 ? true : false;
      if ($scope.schedule) {
        $scope.schedule.visit_start_time = $scope.schedule.visit_start_time * 1000;
        $scope.schedule.visit_end_time = $scope.schedule.visit_end_time * 1000;
        if (!$scope.visitor.name) {
          $scope.visitor.first_name = $scope.schedule.visitor.first_name;
          $scope.visitor.last_name = $scope.schedule.visitor.last_name;
          $scope.visitor.name = $scope.schedule.visitor.name;
        }
      }
    });
  }

  function showOverlay(n) {
    var overlayType = $scope.scanType + 'Overlay';
    return $scope[overlayType]();
  }

  $scope.nextTenant = function() {
    var index = $scope.allSchedules.indexOf($scope.schedule);
    $scope.schedule = (index < $scope.allSchedules.length - 1) ? $scope.allSchedules[index + 1] : _.last($scope.allSchedules);
  };

  $scope.previousTenant = function() {
    var index = $scope.allSchedules.indexOf($scope.schedule);
    $scope.schedule = (index !== 0) ? $scope.allSchedules[index - 1] : _.first($scope.allSchedules);
  };

  function checkLoading(scanType) {
    $scope.noLoading();
    var allBoxes = $('div.box-text');
    var highlightBoxId = scanType + 'Text';
    switch (scanType) {
      case 'DriverLicense':
        $scope.dlLoading = true;
        break;
      case 'Barcode':
        $scope.qrLoading = true;
        break;
      case 'PivCard':
        $scope.pivLoading = true;
        break;
      default:
        $scope.noLoading();
    }
    _.each(allBoxes, function(b) {
      if (b.id == highlightBoxId) b.style.backgroundColor = 'rgba(47,50,56,0.9)';
    });
  }

  $scope.noLoading = function() {
    $scope.pivLoading = false;
    $scope.dlLoading = false;
    $scope.ppLoading = false;
    $scope.qrLoading = false;
  };

  function initiateConfirmButton() {
    Animations.buttonsInit();
    [].slice.call(document.querySelectorAll('.progress-button')).forEach(function(bttn, pos) {
      new UIProgressButton(bttn, {
        callback: function(instance) {
          var progress = 0;
          var progressInterval = $interval(function() {
            progress = Math.min(progress + Math.random() * 0.25, 1);
            instance.setProgress(progress);
            var choice = 1;
            if (progress === 1) {
              instance.stop(choice);
              $interval.cancel(progressInterval);
              if (choice === 1) {
                app.setStore('selected_schedule', $scope.schedule);
                app.setStore('visitor_name', $scope.visitor.name);
                location.href = "#/check_in";
              }
            }
          }, 150);
        }
      });
    });
  }

  $scope.goToTenantCall = function() {
    app.setStore('selected_visitor', $scope.visitor);
    app.setStore('visitor_name', $scope.visitor.name);
    location.href = "#/tenant_call";
  };

  $scope.goToCheckIn = function() {
    app.setStore('selected_schedule', $scope.schedule);
    app.setStore('visitor_name', $scope.visitor.name);
    location.href = "#/check_in";
  };

  $scope.$on('$destroy', function() {
    $('body').children().unbind();
  });

});