// ASSURTEC DRIVER LICENSE SCANNER
kiosk.factory('dlScanner', function($rootScope) {
  var socket = null;
  var timer = null;
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data, callback) {
      socket.emit(eventName, data, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    },
    last: function() {
      return last_scan;
    },
    connect: function() {
      if (typeof io === "undefined" || null === io)
        throw "Socket.io not found!";
      socket = null;
      try {
        socket = io.connect(AppConf.dln_url);
      } catch (e) {
        console.log("Connection Failed");
      }
      return socket;
    },
    getSocket: function() {
      return socket;
    },
    start: function() {
      socket.emit('idscanctl', {
        command: 'start'
      });
    },
    stop: function() {
      socket.emit('idscanctl', {
        command: 'stop'
      });
    },
    status: function() {
      socket.emit('idscanctl', {
        command: 'status'
      });
    },
    restart: function() {
      socket.emit('idscanctl', {
        command: 'stop'
      });
      timer = setTimeout(function() {
        socket.emit('idscanctl', {
          command: 'start'
        });
      }, 3200);
    },
    removeAllListeners: function() {
      socket.removeAllListeners();
      clearTimeout(timer);
    }
  };
});

// ASSURTEC DRIVER LICENSE SCANNER
kiosk.factory('pivScanner', function($rootScope) {
  var socket = null;
  var timer = null;
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data, callback) {
      socket.emit(eventName, data, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    },
    last: function() {
      return last_scan;
    },
    connect: function() {
      if (typeof io === "undefined" || null === io)
        throw "Socket.io not found!";
      socket = null;
      try {
        socket = io.connect(AppConf.piv_url);
      } catch (e) {
        console.log("Connection Failed");
      }
      return socket;
    },
    getSocket: function() {
      return socket;
    },
    removeAllListeners: function() {
      socket.removeAllListeners();
    }
  };
});

// MOTOROLA BARCODE SCANNER
kiosk.factory('motoScanner', function($rootScope) {
  var socket = null;
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data, callback) {
      socket.emit(eventName, data, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    },
    connect: function() {
      socket = netIO.createConnection(AppConf.moto_port, AppConf.moto_host);
      return socket;
    },
    removeAllListeners: function() {
      socket.removeAllListeners();
    }
  };
});

// AUTO DIAL w/TWILIO API
kiosk.factory('autoDial', function($rootScope) {
  var socket = null;
  var timer = null;
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data) {
      socket.emit(eventName, data);
    },
    connect: function() {
      if (typeof io === "undefined" || null === io)
        throw "AutoDial not able to connect!";
      socket = null;
      try {
        socket = io.connect(AppConf.auto_dial_url);
      } catch (e) {
        console.log("AutoDial Connection Failed");
      }
      return socket;
    },
    getSocket: function() {
      return socket;
    },
    removeAllListeners: function() {
      socket.removeAllListeners();
      clearTimeout(timer);
    }
  };
});


// DYMO PRINTER
kiosk.factory('dymoPrinter', function($rootScope) {

  var labelXml = '<?xml version="1.0" encoding="utf-8"?><DieCutLabel Version="8.0" Units="twips"><PaperOrientation>Portrait</PaperOrientation><Id>NameBadge</Id><PaperName>30256 Shipping</PaperName><DrawCommands><Path><FillMode>EvenOdd</FillMode><RoundRectangle X="0" Y="0" Width="3331" Height="5760" Rx="180" Ry="180" /><RoundRectangle X="2880" Y="2520" Width="180" Height="720" Rx="120" Ry="120" /></Path></DrawCommands><ObjectInfo><ShapeObject><Name>SHAPE</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><ShapeType>Rectangle</ShapeType><LineWidth>15</LineWidth><LineAlignment>Center</LineAlignment><FillColor Alpha="0" Red="255" Green="255" Blue="255" /></ShapeObject><Bounds X="82" Y="4530" Width="3192" Height="450" /></ObjectInfo><ObjectInfo><BarcodeObject><Name>BARCODE</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><Text>sv3://6d2f8jbnb8423s79kl</Text><Type>QRCode</Type><Size>Medium</Size><TextPosition>None</TextPosition><TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /><CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /><TextEmbedding>None</TextEmbedding><ECLevel>0</ECLevel><HorizontalAlignment>Center</HorizontalAlignment><QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" /></BarcodeObject><Bounds X="82" Y="5029" Width="684" Height="645" /></ObjectInfo><ObjectInfo><ImageObject><Name>PHOTO</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><Image>iVBORw0KGgoAAAANSUhEUgAAASwAAAF3CAAAAAA6XymdAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAMNSURBVHhe7dChAcAwDMCwrP+f1qNGekDMJWLu7w5b55UFswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCswKzArMCs9Zmfi4pA7pOJks0AAAAAElFTkSuQmCC</Image><ScaleMode>Uniform</ScaleMode><BorderWidth>15</BorderWidth><BorderColor Alpha="255" Red="0" Green="0" Blue="0" /><HorizontalAlignment>Center</HorizontalAlignment><VerticalAlignment>Center</VerticalAlignment></ImageObject><Bounds X="135" Y="336" Width="1575" Height="2100" /></ObjectInfo><ObjectInfo><TextObject><Name>NAME</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><HorizontalAlignment>Left</HorizontalAlignment><VerticalAlignment>Bottom</VerticalAlignment><TextFitMode>AlwaysFit</TextFitMode><UseFullFontHeight>True</UseFullFontHeight><Verticalized>False</Verticalized><StyledText><Element><String>Catherine Hartwell</String><Attributes><Font Family="Arial" Size="12" Bold="False" Italic="False" Underline="False" Strikeout="False" /><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /></Attributes></Element></StyledText></TextObject><Bounds X="1789" Y="1056" Width="1380" Height="1410" /></ObjectInfo><ObjectInfo><TextObject><Name>TEXT_1</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><HorizontalAlignment>Center</HorizontalAlignment><VerticalAlignment>Top</VerticalAlignment><TextFitMode>AlwaysFit</TextFitMode><UseFullFontHeight>True</UseFullFontHeight><Verticalized>False</Verticalized><StyledText><Element><String>VISITOR</String><Attributes><Font Family="Arial" Size="12" Bold="True" Italic="False" Underline="False" Strikeout="False" /><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /></Attributes></Element></StyledText></TextObject><Bounds X="1804" Y="336" Width="1440" Height="420" /></ObjectInfo><ObjectInfo><ImageObject><Name>GRAPHIC_1</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><Image>iVBORw0KGgoAAAANSUhEUgAAAMgAAAA8CAIAAACsOWLGAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAADGnSURBVHhe7Z0JlFXVsbDPeIceaaBpuhllFkEQmRREwTgjKAI+4oCCRoMhMSrROET983yaRM2k+WOCRo0ajD5nRIMDCg6IKAiiMs/Q9ER339t3ONP79qnuSyuQmP+tf61IqNVu6tSuXbuqdu3atc+93epOEGiaZvIfEOKapitUMIVqujzQ6prvBYale47v+64dtTTNDxxXt0MBDA/CsYGpG0Y4IoDga76hqUc98BWb7kPnX+haSDc15NCr8HDyw/CNh3C9QyxcbFaaHxAJpRYQDl31GqaezbqmqduRiON44GD0OVlP061A0z0/kKjyPU8HVKzR6gatbqofzTQ0S9cMvaUNpYdRdRgOFdDdwOEfVjXQ/JZkIf/a/BdmGJ69sFVPhIXKTLpOLvIZpBtEUiqTLojHQYxAI+Yk88FCWpMgI4Bo94UrLM3YPmjOi+HkQXPuVKCC8zB804BIUAcYUdVM2AeKEgacOsD48RRzGB/hShNVhm6oUy3Q8+NxRfE18hGyUulseCRqhkFCklAJo6rVD2GkfsKefTyH4VAB3Qtc9c++BCFgkCdUAIT0XCoDQD3fsw2TNvB821aJLZ1O26ZlWCYpyjBNopBxGceJ2palRqggVKRc9IpAWvUjcZvTgHLNOJyxvulgUDKHVbP8hDWQ+lFrqRY9XF8yVQ6IJzOMKroIJuLBy2TrqqqXL1tWU1Vtmqbr+lnHJVLCmFPHpSrgVaCECepLQQKVXtWGj7m4OwzfeNADT617M6iSqVXuCBG12iCq9lZ4eBaq0IlSswdaojHpOE5NTc2uXZUcfG3btevctUteQYwRTjic+pywYlSrpKjm40kSIUhYcflmSG+uxg7DNxwIrDBy5LghsFoFGf+pbKIgfF0QdtFw3nHzo0hPpvxI1KiqSs596OE1a9aMHj26oqIjodmjR/eevbrFbRUi4fjwnYOSIjlJ0X2VB5sn4x9535Gb/DB804FaO+t7XiqTefWV14/o0Wvw4KM5yyyLotvnUhcEHjWTOrJ0DjVVdnFKJhJNkXiBT5jo2pIPttz3+7l7quqGDBlSW12VH7e7di4tilnxqNGrR+d+fXt1KGvLyRoGmEv0qKsAMWVwSnKeEnAmsletXF2Ql9+71xHN4RvGl8T0IVNgtWzRr8I32kCMOpj+ejZVa8fyqmvqzjxr4u6quhNOOmno0KE9e/bs3rlzSUlxfjQCU1M65XkeScoL/EyT07G8S9oz127e+fQLr7y2ZFnXnn2HHDvC0K3GhtrqXdtq9myLBJmK0qLCPDudrK/o2L5T547lZR2K2xQWFxdGInY6nSE0G+qb6urqN6xdv2XT1vq6vVPOO3fSOeeoVxyHaGD9u4EeOEnNslMpd+ypZ6z8dF37jh2LikuKi4vbFReVl5eXtm1LNBQVFcXz8woKi3UzknW0zzdsXfPFxi82bi8p73rUoOGuEcm6QSwWC1zH1F0vVV+5bcOubZu8bCLftnQ3Q7y5btYPsg2N9YlEA1fIVCrFFaEt0KakfZvi0pKiK2ZeesZZZwaZlB6P/0udiIdkpvl/gJwfvqbheuBnOfRq6tNnTjr/41Vr+w86NuN6nFDRmJ0XjeXFo0A8GnM8N5N1M15QXd/k+GaXI3p3PaK3a9jgkbxCzjSVFTXf8B3Dd4mmwEnX7Nlds2t7Y1W17rm1dVWElK95iWRjJpnkxphfUFCQFyspym9fUlAYsy69YMqUyRPUKzN1fVBF2GH4F4e/f6SQsTKaGane23T6uf/x0eqNY0+f4GpWoBmcfa6TYRiXPks3YnkFph2J5ufnFbcvLe9UU7vXjsbtaDSZSOUXFTqOZ1mW5zmB62m6TykWeC71mUWFnkplkk1NmaZsNpNMNWWyWWSqF6dGYOk+IWgFKStonD7t3PPOPk0PMrZuH74Y/quByhr/ZIbmVuhQhFc1Oqed8x8r1+8Yevw434xrVjQey2P5Y/FoJBJxsm40L59oI7ay4Y0QokzGCUjLNRFZtm1DJBAJSihUZSaBlkxk0ynTNK2InXXVy3k/MDKZjE8IemnfSWjZvYZbO+PCc6edc4aluYZKWc2fiedANscB4e8Y/A9HCYO0/6zjDgYHk3MwZVrz/x2F/yF8HflfB76mPjm2HM9XJjI0L0WGsqJWxnf8wKNKD6Jxz44lAyPh6zVNbqS41IkW1ma8Bs/cC5OnR0zLcTLkJy6PFE8ZJ+36TqD76WwqmWykK2IZJhFLTyZhWg0njOnXq2/7mr1bEk1Vnp+qr6/Lkr4yjpP1Lc3Oj+XHrYgZaJb6EoTKdgcEDPinQL0S+dogG+OA0Mzx/w2ap2lZISECQtwfmrv3g+burw3Nw/aD1r2tcVzU2qU5XHiArzwaXP0hpjNJ9eExKUc3HQqkQPM4zTSPUry0Q5v8goiKGM/TTVvTzaZMWr0nsM10Np11HSiO6xMoRIVNamJUOuGm6i2tqSBfu3DapIumjJo1c/wZY4fG9FQ2VVdQYFHTx2IROxYNDD2dJsgylqSp8KXaAYH0eUBo7t4PmrtDYDPloDVFcAGh7A/N3V8bmqffD5q79wPplSXJLUzrFfoKNA/bD5rV3Q+ah+0HzcP2A7paz57DWVhAeFpLhqE1fw7M226+kUGuHzz62F8qqxrLO/cKjDyftGUHncoK6qs3DRvUJ9VYm2ysLystTTQmLTPKgeZz+9M9JnIDrUkVWHHTj7qpbMzWNDflJKrtoOmYQT1nXX52Y21q6ZIPjxvQbfjAnlE7vm7t+qzj6pZJIe9rQcrJONlEYcwfdnS/I3t2V0pzOh/oWsjxWldXt2fPHtdVH25yr3Rdv6GhMZFoxEiOZpQSm0mHFHzwwF9ZWcnRrPaa79fX1zNq9+7dhYWFcOIOmBELT21trVAaGhpggDkvL6+xsXHHjh1yrCOTscwOA7cOmJHPqO3btzM1PIlEguHwQ+QSTa/yfSvv79q1q6qqirVBWlNTE0PQB90Qy75CIDrTBYMoRptTkhYQc2DgWg3gh507d0KnGgGHDX7UWL16dfv27ZFGFxQZTisIFNQA0AodkskkaiAtHo/DgD7IRxpsKCPDt2zZsmnTpi+++OLTTz/dunXr3r17mYUuXCSShU0gZ686scBq6htPOfv8letrB478lh5pp0Vs32vo0a0DsTWgX7+t23Y3prQ163ZQfrmBHcmLWVEt66hXBtFY3DeivhvEuR5qbiZT52Xru3QoOe+cs7r3KFq48MNXXnz1yJ6dfjrnEs91A8v64NM9//fPT+6saSgobFe5p1aZ0VhdZNR875LJU8efHn6RgsA68L5fsWLF+++///HHH7N43FUJLFzg+y5CKioqjjnmmIkTJ7Zp0yZn4cqVK3HHW2+9xQLAA3Tt2vWMM86As6SkBLfCRsQsXbp02bJleI1lKysrGzp06KhRowYPHrxt27ZFixa9/fbbuJ6x8A8bNmzkyJFDhgxhGfDdZ5999k4IOBoKosaMGXPUUUeNGDEiPz8ffgCns8Ys2BtvvLFmzZqPPvoIZpaEYKJXIomB8APMfuSRRzJFnz59xGSAsfJJ//LlyzH/vffew2oJkeOOOw5Vjz32WOGEiKUzZsyYNGnSzJkzeUQ4NsosOR4U/vzzz5csWUJ8Q8ecs88+u1+/fogNb2DkC8VcU1Pz6quvLly4kC2BhlDwOUOQhv74mRZPXnHFFWgChF5XEM5DxrrlFvKi47jznn6OjNW+rJsdKcwvLGxTkFcQs48fNuRbJw7pVN77i8/WB55hRbgJxjJuNutkbO6EJiHlqw+kfcf005rWpOlNxw4beOEFZ6ZT9tyHnvtg2SrOzL59uo8Y3Dv8Vo3RsUP+gIFDt2zesGbVCourIa73U0XR4Kg+3Qf178/ZTfyjG9ugWc0WQNfS0tKBAweSNhYsWIBrevbs9cMf/nD06FF485VXXiGM2FujR48mheAjvNOxY0ciiQTz8MMPE4vEx+zZs1kMWXVxB67s3LkzYfTCCy988MEHRNWsWbMYxQLgxwEDBiAc/27evBlXXn755XDK5mZs27ZtYSD+YEArEuG1117bu3dvSSFIlnWCE2W6d+9O16pVq1gqlOnWrdsFF1xwwgknEEnQyRmEHb3vvvsuocx+gJ/oZyGRxloyI2HHdDAATEpAXH311cihi+mYBZ1Jt88//zxBTNaEgS6JS1ocCAP6YC9u3Lhx41NPPYU5c+bMwVFwoqSqScLEiYdvuumml156CaMuuugiDEfbyZMnn3jiiYQy0j755BP2OQE3ZcoUBjIc+bSCABwZlE1MqQ4FivdIxMaDJWXl3buVtimye/c+sjCupYuMkSOP37Kjent13a6a2rTHnS4gXThhJjRt3zQo+xOFRdFTzzrz6AEVi9/4fOHf3vbdaH5huZOtTWfcjBMU2panadmMdkSZ+ZPrZsz9U5tXFr6ZbPIL82zXybBanHCWob7rLJp9BXAKwC7p1KlTu3btsA13jBw5nC4yBMOfe+453MF2x37Z4vAQVUQJRwPWoWrfvn1ZJ7pYBlqIADhsPXr0ILCwnXCBiLOQiYsZTiDyyLoeccQR6o7scGuRb71q6NOrVy+IAKGASjJ16GHlYjhBJDI6dOhQXl6O2OLiYjQ566yzEBuK0YgMFunxxx9/5pln2B7V1dUE2a233kpwiIbYjgQ0QR+ChmMaPyCHXoIPNSSU4UQ+0XDvvfcinFVnexC1YjVqoB6ctMQoRBQmfEUHpoAZnieeeOLXv/41Z/SVV1552WWXIZAuGBiI55mR3Tt9+nR2Ee5ClFgqEnKIEa4Y9zHdNwJ+mpzU1l3bPl618pmXXp6/8M03l7z/3if1H67a8dxLLz39/LMffLh069ZN6XSSyLfZiIHnJBvdpmRTfU1Edy695PyePSp+c9/j//38C3a8gAufo1muZmc8P2brnpuxAq0wqkU1La5p37ts0iXnn1MSt/xsSt0T8L66MQAHPgdxGZ7CbNYVC1EapKEhAY6pJHMsxKFkMgkaAIMBqiI4ca74VJnbqiAlIFgPKPiUpUUUdGHgUUTBxnCEgCMw50oGIhCVGAVDKDhg09PFQOj0Cic4QuiFU9h4bE1BFOvHKt51113kMB7XrVt3++2308ImViMTTtabFrFCpJeoEgoTAZRNANKIrSeffBKKGAgCP0jOk7AxFkR0RgImz58//7777qPrvPPO45jDLXShJL0YLmrQssd+/OMfQ6QLCYiVKQSgcxlTXzZ29YD7nR84uqlZsSg/kbz8Dp26LX7vo5Vr1r2x+J1sYHTo3Am2SNSycIifrarclthbmR/xY4ZbaOtRwyuKmW8ten/dhs2FbdqmOJUtk9tlKpvJLyxQy6W+bOq76TSaFtla3NAunHxyeYcSL5OKqw8Q0yROcuY+7b4MaI9JgLgJwE7chw1Q2LtsPo4YMgHMEiLQweFhLB6EE0R8AYgjWHtaiDgLftnZPOZa2GQgXeCwAble9AFhnQhQEc5KIBweURUiowQBZJ0kCkHgZKzw8wjD0UcfTaKScFm/fv2zzz5LJCEK+aIJXVgHAlEkiCgBHmlJJMJDiDz22GOiJF3MLooxBIGiIS102Z+Mgp8qkPRM4ocHaWgrnhRAFABCNuUoFyKAzNaaGG6YJRhpRkzNCBwv2+RkUg6FdmzVmnVb99RU7W2qaUh9tmnzzuo6zbIzmVQ62VBbtXPUyGPOOmWU6TVqqb1eoo4VjuhafqTYtgoC3SYFeoZr8K+lk7dRxLSom7RYxNb8TOClbS1Ip33bUsGGPeqrpvCobzIfGLBfjM9pzyhWQlzJVWXDhg2YetJJJ0ERr+FEcFqsk5UT/ypjW8oCECUrlJ9zjSx2joEucCSgANEMhCNUHNDCKZkJABE8xwMuiMhXltq2qAcRTSDSAnCKVtTREyZMQA6cr732GqaJBDghijJiOAw5/UUZ5FBKjh07dty4cTCTin7+85/PmzePXjihMLVoKHOJ8gwERz71GZcY4oz9yclOLwMlvISTGUVhUWDatGnoIyoJKFVCMCwOQ0/zso7mu7rKkUpRli+RaDJJXLGirTt3JzOeYUZdT70WSzYmYpZ5ycXfPuNbYyacOex737mwJE+z/YzlZdUv6/imrr63bBMjjueksuTURv7Di6Zu4RjufCpLqknUl3NsU50mLJ1SBd+pb9cc9FUWeqMehqEGFvJIRYjZXJ4psChjqUMpp7ATBmFuMac5XeMRKOAQpaULupLeAkIHkEMX/LLLQZAJhSHCpjwa8iCWFgo84BCFgVaGyKOwSbjniLQAbBDpgo5pp5xyCkRy7Y4dOyi2wIVfVpRWdBBiLlDAkUCCofy6+eabuYhAp3zkeH300UeFX5QEQQgIqopMEUuVyeysF7dsKjmIoYlqi8IMICSnJ484X8YCUAAYBNRX0QPHz1Mv0T3OWLYqNXbEMNULS1/3HG/X9spkQ1J9vuz6e6vqjhk4ePZV38uLRuc+cN/WjZuG9S+95rszjurRyWms5V5IVkI4mcXJeolkQ31DXSrFWI8JmVm3IlwMKXPCb/mZrq+56nxkMjvw1PdJTfm9xIMAbpIWM9hVXKOuv/6G6667jnscd7pzzjmHqz7W4mgYZAgAP66hzZktvSINb5JFQMQv4mUekcMKCQ/D8SZ0HgWESAvwSBdjoeTmBeERTcBluPQiFoRWpgMXaQIyO3Qig6VFAc5fYgsekUCX4EhoHtPySZrgdFHgs9MIi5/+9KeDBg0igRGgv/zlL+fOnQsn8sUupAG4UekRRjPFKJcGJDAp4Yg06MQQCIqJcFrGwiyGSBcOFFxaMcHQXM2MGE42HeXQ0oJsU1MmlXQyWVaasxYptImGxohlIuzymZdNOuesRa8v+tPcBxP1e/Oi6gt7/Y8o+c7080eNODovqqVT6tVfY2Oypm5vqilD+UbG1AMP5+ETZmV2UhaBxQpACLOgMoCJUAnlQ54Dg2iMJVglF+MuXbqUlpZyVQHnLnPNNdewvzEPaXDmgEcAOjhyxE3CAyIVhmw+/EivTCRFFSDDAXClRwg5HLo8yrwyVrrQE2/IdhcKXQLgMjC3DeARTuQwNZEhFNxPL0RECSegtGkVkQAMtPBIJQ4noXnbbbdRtEEhFH71q189/PDDdBFntMQfasCPHJFMwMEJEVyE0IVzQFADIp6RdyuLFi167733Fi9eTIZ7/fXXly9fTnUr7kWU+MFQLxxIKNQQhIDrIii8ZdCtRaJGJtuk6VnD9Bvqa84ef0bnio733vO7dxa/U5BXVFxUQk2BccRxl46FF3/7nA7ttYaGyvr6WurN8NdybCftB254DKEdIRUedYFKVy2BpV7AB7rPnUF9XUZ1Hvx1g2gMSHU1ePDg2bOvuv766++8887Zs2ez4TCSI+CTTz5hSYQTYKwsHhJ4BMdNOZnQBYdOL34Bp5XpZBQUkbM/CBsInMIPrmYN+WnRROJYHuEXgQLw08tAVGJSgpvVFVGEgmQslgNOKIgCZOD+IKGQO5uQBnB3u+OOO3r37g0FOeStBx98EDYeEY40+HlEOLqhCZOK5k3hZwM80uJt2OBhCEJIZgsWLPjRj37EpfWmm24iwrBINiFjZTh4uJpoa+imbTGoroHcWU+t5LipZNNeC5OzyZjl50fNTh3afLj03cpdOzp36mroVirBZYGCX4txgPlenDqd+Aw43Ny0w60H6abn6urrM0zZam0Ul2o1oilkw910cjvlXtjMsz/AIUayBnIZVJm0Qe1mNvf06dO5HhcWFm7atOk3v/kNfoGO+2jxl5Ie7n68z6MsD70oluMR9+EgoQD0fmUh95kQavwVBEDD1jw8gkAhagUJe/YBPLTMyET08ihxhp7sEzY5CMEhYZfj3x8Yy0CZkVYuktAxqkePHnffffeAAQOYBev+8Ic//P73v6eXmyOTMgrdaGHGjcQNEnjcunUrVy6I8OAWmR19OBzGjBlz8sknI6GyshIi9fvo0aNlII/MzkToY/gW2Ulvymaox9IcBHaksE0hS9CYqO3Xt9usWTMu+va5iYbdutdguE2FUb1NUXGiKZXOuKkMWVf9Jk4qmbEMKpIEVzo2reuqqz6+QCSFGomQyULf858CLNbV1+XDF1bhLyfm1vLvADwYLDtMtMdm8jkTgbC3zjzzzOLiYvAtW7Z88cUXsImXJQdDZ4gEHERaHAoRmdDh5BF3IEGWBFw8tT/Q1YyFuLKu5ZylBUclWgCKCAEXfkC6xOQcwkAAHH0IJvSU2gjNjzzySKyjNxz9JRBDBMRAQgoEBcQtuAuEaw31FnLAkXP//fc/++yzkpwkqkRPEIKYGXnEh+vXr4dffAgnCC3yQahAiEIQ/M8UCJHpRA2mgKKSMM+Io+72s15eJFpfU2Ob2oXTJp885viays0zphx3243XtCuwGvdsszW3qroymUpVV9em01nTiGBZPrWVr8Vi+ZieTGeSqQxuDlT9hGz1iZhHTlJOVPaH34qh5OLHBffDgxISKZByS3EcHDCMFjvVIoQLyRrgIKYB79mzZ+fOncHZprt27UIsRjIE09hnpDeGUArASZcIQRragcBJ3YpnO3TokJtIPCXMIPsDdDhhEAnKmy0VOr1IRqDgtCJEhkgvLcAQEQIFNvRnJ3AvqaurowYfP358TiV4aA+mjAynFQYRCMhxRmxxDg4fPhyZ5Kqf/exnlKRIo1eGi+TTTz+dQEEODuSijW54mC4QYRN+gp6bo4QmCGaCy4y0MEBRF35Mj6pPkLXCWF62salLWflVl82I+t5Tf35w+eI3MyntjON63X7dD3uXlzbu2eFmU43JhG6Ze+vrsxx16gUaDtNSKReDXLVXmdvIZgmpjOdncDdz0aXsDSPH0DxDffUqMAksz9FUaa8uvZxXoc8OsDUBbBOXIRADaMFxgVjCrKyHLAxGcqvKuaOkpGTgwIGhGv6yZcsQIt6BAVzk7Nmzhw3aq1cvtjWPMhAGWhhgE1zmyj0KiAQolCCioagqOOcFiwQij0iQeWUzQAHAZUZRnpZFpaU2mjp1KnTspWWsDMkN/MqjAPsKTSSmESvCGcvuoiqiYNi7dy+PzzzzDJqQFMUo0Xno0KHjxo2DQvC9+OKL8+fPZ1vyiAIyC0MQC5FWPEPSgk780UKEQY4F9ovBHZBLX/u26nVtTU3NiGHH1tfWPTz3j9u3bM2L2HmcZ7425KjyUSP6cHl0AzfjZCnITDvie6omkjqEABVney6FoaqEDFNzPJVL0QyllGLqN1MDropEEtU643QvawYeoY0jwtrrwFElgExEoX0YhaqqiEbVpgGhl+wtXyMhOPr16wcdZizC8mHDhlF+gVPdwwYPvbiSlnCE7amnnmLsMccc07dvXxSWZRbHiU/hhNLaxTkQAwGWU8bCwCNaIeHDEGATHgBcxAoOEExQxC4QSqJ3332X/cCNhFhn6tyMrXEBeaRlXqaDAWlYiq9kCvEVveBlZWXcEwkdVJWAyA0XCfBce+21uIsEj2fuuuuup59+GnpONzVlaItQQARgho0ZmYuwg0d5EF8RauXlHX3dT2XTqUymsrrKCYxINJ5NO7auPt1j2XFDkurZ94yInUimEIcUhpvI0AMrDAmK98DLug7VvJnJhnW7q34Ng87AUF9V5gboh9EYeFqE+PIylp61Da9dW/UdJrpDlZQshjKFEtpiPy5De6paTnqs2rJlExdAfIQTSTmUDlTu7PJZs2ZhJ0PgkQrg7LPPnjhxItpu3rz5hhtuWL16tewqBrKRKGZ/97vfUYFeeeWVEJmChYFZ9h97UW0SwyD/I01MBsSJtBJPdBGy1C60nKpkBfjfeecdThx2P/yywMwrA1esWIG24AjnkZZi+f3337/44osfffRRrOA2R42MFUwtS0gLP0ePSGMK0RAeWgBOeiFiFFaDhJoqwCIeGc6djnNwwoQJ4kOIjEICXbCByEEpmRJXo8acOXPwM5JFGTihg0NBbQkDiNKGsyk5FmdRLEbkuN26d8rLiycDJ51pikfV5YhlVbKIAvX1B/UCwdcMbn3U4/zQTa8SoyOuJQI8iibH9NQz2hoaCT8cHEL4l9b4USIp40ifluFrXioWNSs6lnl+QOAI5/6Ap7CEqly+kMS24Oby3e9+l2MO70DHd5MnT7700kv79OkDgzhazGb4ddddxx5988034Zw0adKQIUOoQFl7TkAWm6vNVVddRZJACOFLlOAjepnirbfewn24m5Q2b968kSNHUsm1b9+eSVnaTz/9lCzIgcJcRBhLcs899xDW3L/Y9NRJxx57LPcyHMWByPJ88MEHlL2sKMF3yimnHH/88TlRO3bsQJPy8nJUpbQiu0hAMxZl4Nm+ffvnn3++fPlyhGMg0kgnGEsU0st02PLGG29gMsH9+OOPjx07VqpsYggNMQ27cAXtjTfeiDfYiq2PMEFgI7v/5Cc/QbdXX30VmQsXLuRYxGrSuUjDasxhXpyP09AQYDiukwhDbT3gmKKWdtNvLH73klk/qkka555/WftO3f/44IPxSDBqSO9HfndHsa2+gVfvaz/6zz88uWBJ2w7dUk2JdjH/Nz+9+qQhR7CP1Gd8XpC2zBvufeLPL74ZySv3VE5yyUZuY+1pxw+Y+/Mf2OpXuzgKiUK4TSPQ2L0zvjMLL4wbO3rOtVcXFuZjIFEoRoquORyEeoUcgPG4SUIHb1KN4inWgxtN//79YWZ5JBsLYCorxGKAb9y4kfUgb+EXiGzfiooKPHj00UcLM4DjEMgsrL0EDRPJ+cIy4DKCkrlQadu2bR9//DELCb9kU7QC2NC0SGAKvD9q1CjEMu/atWuRhm7qw1NdJ69wmQCHE2LHjh1hBiTDSTAhHAThTMdybtiwgS6xBdthYGpilyika9WqVew95KAnYgkCDnceYQaQgxBiWiKMbfDYY4+NGDGC4MjNIpw8YosgiCW28Nju3bsxSnyLhpzR1Bu0MgpOWowCQNR6edTc4EEmkcp+a/zUT9bumDRtZqy47PF5f7WtYPTQfn9+4M42jJXAuv33f3n5nZKyLulEY9u4dv8dV580tCfXOcIkcP2sbc25+7FHnn89mt/R02zfdSKmm63fc8bowQ/d/UMJLAQxH4FFms462oXTL0f12bMumzZtaiRi+QFF/T7lRFfBcQeZABwX0OLT3BbEYPwFIh7hEX4MxstSSQDiEej05jyYA1ZCiOJQgJjY39HKX2HpBkILg7DJo6iEJjDnhghFmRQaAl1JbFkDHgURydKFTERBoRXTCAIsFdN4zNmLcIitKa1B/MNYAhED8V6Ov5kj5BH9c2qgkkyNn6HATwtnzqIcyKStZbbmUUcbOLf9ooLCE0cfp7nJ9sXxuOVyqhHi9PpyDlIPUWaFoR1CswYiJfcRH0Tmo5uo4kyUR57hgxkx4Tmo5qa4ZOFsyygqKiDHRiMWNz31kvRAwFCJKgSGHlClDx6hFcNoBQcwEmAIUQWRdWJgLqpk+cFpxQQYEI5MkQMP/LK0SvnwOgmd0KSlS3zH7HQJDxIYTisLqcwPa3mGSEDAD0BHHx7pBegVIoBkMiUSkMbU8EhUyVgiA0S6YJMhSKALS0Ggi5nIB9BKYhGcsTCgLVNAgR85AghhIJozkBYGwcV7tKKDVFfQZQgtYkEg0oqBogyIOEThbCf+gYX/zp98bklRNFFf6aQTNuGL8mE1Jn9vSCGkN4hKPqFDeIcdQUs3blJfLKV+V2YrplAPmTX8W0bNLuYf1N62bdfGjeuPHnhU//5HhgqpZdsfxCpafCeORqyEAv7CMOTjBSg4UbyZGwU/PCB4DQpsjIIHCeIRFkAYxB0Q4ZHhOX4ZQmgyBetKLx5HgtCFX9hAJILBUQYAl7BDPqM4ChnIEOjgIgo1kMwjCF20DBE2cGYE5OCTUTDTi0BmR4JoIoFOK72ileAIAYcZJ4gP6QWQhky6QACRJhSCnhJTnMP+lJjDz+Jq0ZkusQuc4eJAKCLBvPXW21ASRmhcDD9f+8Wq1aurq2tr9ybZzp0qSiede0ocxcLP9RYsXPrJ2s2RvCLyQ56ljf/W8T06t1UfKau/TaO+MvPy4o8+XLXWjhSoDwFdh3I/k2zo071i0pmj1UsqZaB8JqhOxft++9v33l966aWXDBk8gPF2RC606LYvUnM42oN/9tlnVK/UDZgnFOwRH+GypUuX4ndqC+xnyEsvvcTSstLiC7wDwhDK/zVr1lA3LFu2jKp53bp17dq1w32sgcgRx8EGA5UWtVTXrl1lhfCmeBkeaqa33367rfrzFsUMhIIygohi8vsIVHIUJdAZzlgkgItFrN/8+fPB5Y2/hIgIhwiycuVKSgUqQm6yVPeogXpiDr3CQ9gtWLCASwlsGI4t0EUZZIIDH374IZNSaIsOXD+5R1OPi8m56XgEuK+gOXcF6kL806lTJ3SGIedGdEACJR26EX+lpaUwIFlcJ4qxNuHiEY/ZtB44V8y8JMg0JeqqbIPy25W/awXI8uqB+gRGfW6svuupICTvA/aqvEwHMEz0Fja6+BE2YNXKTx566CEuWaeeemoL1746Y3/AHlQHnnzySThBIIJIVDGQDcflhesM5tGLhYQOZrNrocAMIlpR6rJCuIw7GgiLihAYxCmw0SKTqCL+uNOxHlDgwRzYZGfDwBnxyiuvSBzDgGR4RDEeJQRZcsmg0GFjLGz0MhcS2AZI4PIoMmV5YIAThOsYAdGtWzdqcIBKH53ZKjAwFgaRzCjim0f5ap7MRcsUOZxLzy233MIlFwYkgLBboCNH2FAJbcEfeeQRwp3bydChQ5mamh0JYqAIB9irixcvZl9Rv+MclIcBuogSvDmiCRw2I6ShgwdNPW9CUdwqbRP3nCbPzYR/Pk39/iqj0CNw3CA8ILFKlObcU3qFMSFz01J3UJMFhBZHgEqViqi+1RCoWgTVb77lxlQqefnMGUUFrLdrhGcOY6XNIUwoOO6TJWGlaaGLtbRKqxCGDx+OnXgQTkaRJ+S4YWxOMdYeCXKjgQGcUfL1I0SJReIaiT9u7PIJLhQBZUiY/BlF7swtDCAIXTIdCwADkaqGtdgCv9K1RX/us6gHXQwBQUPwnTt3snjjxo1DByZijadOnUp2hCGcShkuQYZ86KSNnj17YhcMMjs8uFpc0adPH6z4xS9+QWjy2L59+8LCQqZDGTENNtRYsWIF+Xv8+PGozaTE1oQJE2BDFDyt44bdSBzjPa6xPAq0xIOa3XA5B+U9ldKVVBxccP7U/n17dOnYLm5yacymVVGhmRpahohNLk2TlkwOT/XtvWZx4hQ1a3j8Ix0cDyO8sbGes9K2lUPREAP+6z/vePnll2fPns2iUkNLwqAoUxMcBBgrbOxvLARkdWX20NXq9ya+//3v42ViC5/iO6IfaFFGXRVBAHqpWmjBGSgKIwoEIghEEG74zz///N/+9rfcLLRMzSgotMwouNARwiha4RRpKAAOAgMIQC/AI63kCYgyqRw0WMq5xiPhIrrJb6GhM4/CD5FWfMKQJUuW/OUvf+Hw4hEGJkUx7IUBOTht+vTpgwYNuv3226mc2FHIwSEA/LSwwU+GZjuBiAnQZS5wsQIgmmGAQsaaN2/ea6+9xuzQ0Rw6iDhZfVaoSOG7AMuOZLKZioqOM6dfVNa2YEDf7u1LCtrkafgGz2ECc7H+6s8VqTBAN5eIUaa1xIRycehTpbGuantylyogyHApdeEizT/yp4fvvPPOsWPHXnrJxdGoVHdqiLQHAyRjLaoijRaK8kpLRc9YRMOAVXPmzMH4+++/Hx4KDkkY0OHBpxIEEHElgOMYJQh04UQsU8BJDmDLcl5Dh00cB8AgFLY1/Ewtj0yBHCiMhUemIG+hJER4kCBjocAPIvqAi1EcKwiBjZAitjZt2gQFmZREhDjlFAJbr59auzCyTz755ClTpsibPIRjoOjAo/ATlDNmzCDBUC2Qt+SuIEbRMoRZSM9UsdBFB9wFAmAgAplLwgsGgOPyvPPOGzNmjGiCnhDpFdepublJGKbu+dRN4Sfznjti2JDzz5vYuaykvH1x5e5qrkOup3EfUh7UZYMqB4kg9UFgy4UTJfhPyUU5jsLAi0Sj9fV1qbTaW7Goze7/wQ9+0LVr51/fe095RQW3e8oSWRiG0bYGJa0FKGjQDe2ppmlzLsMq/ILByEeO+OK6665jDajhOHOxk4GiHgEXClOSYZa1ZIgQ4UEIksVNstg8AiKWXtigg0MERxkQ+EldzCKGQ2cgPAAMaIKSpBw1R8sywwyAoDmagMvyIBycgZyAgwcPJgkhATayLyCq0iuTMossZyOHQn096gmzSEaseAYGEAILhFOC3fLggw8KDxQQBopWw4YNI4Duu+8+HpmC4COHgTAjEpiLXgSC41jZLeJh8Rt0UU9J9pywLvPVQeCxtqatGZabyZx28uhpkydoTvLlF59PNDbYJrs8/Og4/B+VBL5r6oFtGV6gnAgVuXhaNSjVEmSAm01HI3ZeTH0fdcHLCy+bORN1Maz/wIE4GYGA8KMMJoTIPhAhALc2hLPVSBK0DIEIgzgOg8n2+L2yspIueK699lo2MekN42HDCzILCPyIohYWv5BR6AJEYA6Bh3WiLn733XehMEp6QXikF4SxlOdcGlgANIGB5ZQucDxDNHz00Uec+/LZEXQAISIKndFcDjjoIpZWYuLCCy/s27cvxw3Dqb7B5WSEAYBfYhFDuECQ215//XXyDZJFOIsNIJ95iTwuekK/4oorTjvtNPkohqjCIdAJGlo2HvkMngceeIAESSHPKElmgJLbsiVYDq6N2M71UOgiB0ArlFcf6RD8Gb+JvBVokFxyqJ/JGtGI4zqvLXrrvfeWdinvOHnKNDdacO0td//5vxe2q+iWbkrHjcyj99168nH9OUvYY5xxJJ9Z/+eBPz3xklVYrgWWoX5fx2mq2znptFGP/PKWZx5/7IYf30QRH36MdULgZNXH10Z4w2ItNV83mu93ot9XAF0xGLcCIISOrIR0gYMQB1QPIbtKkPRK3MjOhshY6NJCZEXpzaUxgC68hkAYJPLIlExXUVHRWjdwWuIYbyIHhCVnCZVDwyVnCDw8SiKBh1mIckYxBTzCxiOpjhZzoIhkGS48wkC269ChA3IIIFFDTBYEhTETW1AGc4iYnL0wIwoGhGALEEpVU6MzCoMzCjbpEs8wkDBl/7NV5PIBHaJ0CQ/T0Su2oBuziDkAOK1520236ib5UE+5WZZZVQd+oKukpHJSjx7dC+Lxzes37qqsLmpfvnDRO2vWb7Ej6ks8Ucsff8roXt3KGOI6Wd0wkT3/reUrVq+zo0WkH8zSvIzupsvbFtTs2PrLn9/ZrVv3hx6cO/qE4zmsLZv0q+6TBmZyonts2eZXIAcE9MJB2E+L+3iUlVP6hr5jrGQ18ZHQBadX1gMiQ/ALjzk50HGQrJA80sJAL8PxLwEhE0EEYBBAK9aPmJD1luEQpUtE0QsdCRJ2ADKli0kZhXz0gUdJbMlYIgQcE2DgEsdEhCZEKAwHGC4zIhMJclbCzCgxEDpy4GEIY0GEHyK9qCGzCB2KPBKUDCc60ZxdCk4IMjsDRR8o0uIfADZCk0eGE2ogCAFaoiwIolaUZKfEMzfT8MOEujF82LCJEycW5OWtWrH8iM5lbWJmqrbSbaxtE49GTNt1VI1l2VFHfeioWT4/fsx3o37GzDYG6fqSPGPnlrV/nffY6NGjH3304eEjhqFHuJ8Cznb5n1wwJ+WTOOLvAPbIdsl5QewRigzHfnG94PDQBeQQbKILkEd4xC84Gp/m6PALG10wiFshCp0peGQIveCytDk6iLSiDMbKWJlF+EUyyoCLSkKEU5hpeaQLkxmiliIkgqAPXSJKhkBEeWEQOYDIBxHlhQjAJoqBMCo3Ly10NoAIoRWiZHRw+HMCRR9w1EMTUYmxOZXU+0z1lhSVIMircYWpP6bAXRlUHZGeX11bt3NPVV1Dqq4++9Qz8+e/9DdT15584k9jTzwyFMVBZRiWfs2Pf/vXp1+O5xfX7d0by9O7VJSUt4sbXmrCWadecP5UAhwNRA9q9lCzMLLDuVFHtdwzD8M3H1RqVf+0bFBA4pS1D/NKc01AhHK7qdvb0Jh027UrX/TGkhdefPGiCy84cdxgEiRx7KhvC5rXX3/PH/74aH5hMWdon/7ddT/ZtbztxPGnDxkyhOuiF4pCpgT+PpDAEjgcV4cENKdcaVsDFDIeiGRFSX1EoeNqdfUNxQVtDEtTfxfXcEw9o3OV5D89evON//Xh8tXHjx6NgPyiyMABfUcNH9KmUL3X5ioQsZovaEgT4c1wOLAOOWjOWK1BgoyFJ1eBc1TTSv3BKeZ5DhW3+kMMmqG+yuBnEo216pMa3dy+s2rThp2ZrO84XllZ6ZDhg4sK4px46fAvx5H/1LEbCqGVMx4EUIdvLmUeDqxDAr4UWAQQj9KSqGiJLUKKljTDUZhON2XUF4e4QRI8LtFBSMWidlGb4nQqu337rqo9ddwpS0vL2pa2pYqyuAAFRnFhEbcVYosyUBCmkItGDlqfxYfhEIDmoxBoHU8AV0coxFAymSS2CKyE+s3jhnQqyb91dbXVNeovvUIkAxUWFJeWdvj0088YzN2b6MkvLMiPq1/oI8i4kVK2c2WADjPhBY/Ek9xcBBc1DsOhAV8KLICQEgQgmMhVEmfgkroSDUkSWdZp8jz1+QlEbqyZNBGl/s6Ebeuup14G5hcUEDJ2JEY8IZz8BBHgUWo1QgqEeCKwQABR4zAcGrAvsA4GwhBGGmGnmToJRktnktGo+n6SrluuY1gmUaKY05kUJ6AdUa9VTEO9CfQC9ZYyHKtexoRRqqJKce8HsB2OsEMD/nFgfQWafw9eV//PnPDZUl9NDn9BIqSHP/L1QPmLI4fj5N8S/jeVjUTkVy+VIRyQeBj+jeCfDyxG8KPeEISBBRJSVNqStjmqwl/2Opyu/l3hf5OxgJbM9KUAyqWrw3nr3xU07X8AEYeBrc4dtqUAAAAASUVORK5CYII=</Image><ScaleMode>Uniform</ScaleMode><BorderWidth>0</BorderWidth><BorderColor Alpha="255" Red="0" Green="0" Blue="0" /><HorizontalAlignment>Right</HorizontalAlignment><VerticalAlignment>Bottom</VerticalAlignment></ImageObject><Bounds X="810" Y="5104" Width="2419" Height="570" /></ObjectInfo><ObjectInfo><ShapeObject><Name>SHAPE_1</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><ShapeType>HorizontalLine</ShapeType><LineWidth>15</LineWidth><LineAlignment>Center</LineAlignment><FillColor Alpha="0" Red="255" Green="255" Blue="255" /></ShapeObject><Bounds X="105" Y="2505" Width="3135" Height="15" /></ObjectInfo><ObjectInfo><TextObject><Name>TENANT</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><HorizontalAlignment>Center</HorizontalAlignment><VerticalAlignment>Middle</VerticalAlignment><TextFitMode>ShrinkToFit</TextFitMode><UseFullFontHeight>True</UseFullFontHeight><Verticalized>False</Verticalized><StyledText><Element><String>Richards Frank &amp; Partners LLP</String><Attributes><Font Family="Arial" Size="16" Bold="True" Italic="False" Underline="False" Strikeout="False" /><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /></Attributes></Element></StyledText></TextObject><Bounds X="82" Y="2805" Width="3162" Height="285" /></ObjectInfo><ObjectInfo><TextObject><Name>ACCESS</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><HorizontalAlignment>Center</HorizontalAlignment><VerticalAlignment>Middle</VerticalAlignment><TextFitMode>ShrinkToFit</TextFitMode><UseFullFontHeight>True</UseFullFontHeight><Verticalized>False</Verticalized><StyledText><Element><String>Booth 6039 (ISC West)</String><Attributes><Font Family="Arial" Size="14" Bold="True" Italic="False" Underline="False" Strikeout="False" /><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /></Attributes></Element></StyledText></TextObject><Bounds X="154" Y="3210" Width="3060" Height="1200" /></ObjectInfo><ObjectInfo><TextObject><Name>TIME</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><HorizontalAlignment>Right</HorizontalAlignment><VerticalAlignment>Middle</VerticalAlignment><TextFitMode>ShrinkToFit</TextFitMode><UseFullFontHeight>True</UseFullFontHeight><Verticalized>False</Verticalized><StyledText><Element><String>2:45 PM</String><Attributes><Font Family="Arial" Size="12" Bold="True" Italic="False" Underline="False" Strikeout="False" /><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /></Attributes></Element></StyledText></TextObject><Bounds X="2086" Y="4587" Width="1104" Height="348" /></ObjectInfo><ObjectInfo><TextObject><Name>DATE</Name><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /><BackColor Alpha="0" Red="255" Green="255" Blue="255" /><LinkedObjectName></LinkedObjectName><Rotation>Rotation0</Rotation><IsMirrored>False</IsMirrored><IsVariable>False</IsVariable><HorizontalAlignment>Left</HorizontalAlignment><VerticalAlignment>Middle</VerticalAlignment><TextFitMode>ShrinkToFit</TextFitMode><UseFullFontHeight>True</UseFullFontHeight><Verticalized>False</Verticalized><StyledText><Element><String>14 October 2014</String><Attributes><Font Family="Arial" Size="12" Bold="True" Italic="False" Underline="False" Strikeout="False" /><ForeColor Alpha="255" Red="0" Green="0" Blue="0" /></Attributes></Element></StyledText></TextObject><Bounds X="144" Y="4584" Width="2088" Height="336.000000000001" /></ObjectInfo></DieCutLabel>';
  var label = dymo.label.framework.openLabelXml(labelXml);

  return {
    print: function(visit) {
      var labelSet = new dymo.label.framework.LabelSetBuilder();
      var record = labelSet.addRecord();

      // set label text
      var d = new Date();
      var date = d.getMonth() + 1 + '/' + d.getDate() + '/' + d.getFullYear();
      var time = d.getHours() + ':' + (d.getMinutes() > 9 ? d.getMinutes() : '0' + d.getMinutes());
      var b64 = visit.image.replace(/^data:.*;base64,/, '');

      record.setText("NAME", visit.visitor.name.toString());
      record.setText("TENANT", visit.tenant.name.toString());
      record.setText("BARCODE", (visit.barcode || "").toString());
      if (b64) record.setText("PHOTO", b64.toString());
      record.setText("DATE", date.toString());
      record.setText("TIME", time.toString());
      // record.setText("ACCESS", (visit.floor.name || "") + " " + (visit.space.name || ""));

      var printers = dymo.label.framework.getPrinters();
      if (printers.length == 0) {
        throw "No DYMO printers are installed. Install DYMO printers.";
      }

      var printerName = "";
      for (var i = 0; i < printers.length; ++i) {
        var printer = printers[i];
        if (printer.printerType == "LabelWriterPrinter" && printer.isConnected) {
          printerName = printer.name;
          break;
        }
      }

      if (printerName == "") {
        throw "No LabelWriter printers found. Install LabelWriter printer";
      }

      // finally print the label
      label.print(printerName, '', labelSet);
    }

  };
});